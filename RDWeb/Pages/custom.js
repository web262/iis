function addAutoConnection() {
    function newAutoconnectionCheckbox(checkBoxName) {
        var label = document.createElement("label")
        var input = document.createElement("input")
        label.className = "autoConnection"
        input.type = "checkbox";
        input.name = checkBoxName
        input.checked = (localStorage.getItem(("autoCon_" + checkBoxName)) === "true")
        input.className = "autoConnectionChck"
        label.appendChild(input)
        label.appendChild(document.createTextNode("AutoConn"));
        return label
    }

    function newCustomDiv(targetElement) {
		if (document.querySelector(".CustomDiv")){
			return
		}
        var rpdConnectionDiv = document.querySelectorAll(".tswa_boss")
        for (var i = 0; i < rpdConnectionDiv.length; i++) {
            var customDiv = document.createElement('div')
            customDiv.className = "CustomDiv"
            targetElement.appendChild(customDiv)
            customDiv.appendChild(rpdConnectionDiv[i])
            customDiv.appendChild(newAutoconnectionCheckbox(rpdConnectionDiv[i].title))
        }
    }

    function startAutoConnect() {
		if (sessionStorage.getItem('autoConnected')  === "true"){
			return
		}
        var localStorageKeys = Object.keys(localStorage)
        var autoConnection = localStorageKeys.filter(function(e) {
            return e.indexOf("autoCon_") > -1 && localStorage.getItem(e)  === "true"
        })
        if (autoConnection.length === 0) {
            return
        }
        for (var i = 0; i < autoConnection.length; i++) {
            var appName = autoConnection[i].replace("autoCon_", "");
            (document.querySelector(".tswa_boss[title='" + appName + "']")).onmouseup();
        }
		sessionStorage.setItem("autoConnected",true)
    }
	var tswa_appboard = document.querySelector("[class$='tswa_appdisplay']")
    newCustomDiv(tswa_appboard)
    startAutoConnect();
    document.body.addEventListener("change", function(event) {
        if (event.target.className !== "autoConnectionChck") {
            return
        }
        localStorage.setItem(("autoCon_" + event.target.name), event.target.checked)
    });
}

function restoreMultiMonState() {
    var multiMonChk = document.querySelector("[id$='chkShowOptimizeExperience']")
    multiMonChk.checked = (localStorage.getItem('multiMon') === "true")
    multiMonChk.addEventListener('change', function() {
        localStorage.setItem('multiMon', this.checked)
    })
}